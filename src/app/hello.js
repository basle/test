(function () {
  var app = angular.module('usersViewer', []);

  var UsersController = function ($scope, $http) {
    var onUsers = function (response) {
      $scope.users = response.data;
    };

    $http.get("https://jsonplaceholder.typicode.com/users")
         .then(onUsers);
  };
  app.controller("UsersController", ["$scope", "$http", UsersController]);
  app.directive("userTable", function () {
    return {
      templateUrl: 'app/hello.html'
    };
  });
  // .component('app', {
  //   templateUrl: 'app/hello.html'
  // });
})();
